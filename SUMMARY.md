# Table of contents

* [Airdrop and Bounty](README.md)

## Artificial Inteligence

* [Kuliah Kecerdasan Buatan](artificial-inteligence/kuliah-kecerdasan-buatan.md)

## Pemrograman Perangkat Bergerak

* [Ringkasan](pemrograman-perangkat-bergerak/ringkasan.md)
* [Komponen Arsitektur Android](pemrograman-perangkat-bergerak/komponen-arsitektur-android.md)

## Catatan Aqidah

* [Menuntut Ilmu Jalan Menuju Surga](catatan-aqidah/menuntut-ilmu-jalan-menuju-surga.md)

