---
description: >-
  Komponen arsitektur Android adalah kumpulan library untuk membantu Anda
  mendesain aplikasi yang tangguh, dapat diuji, dan mudah pemeliharaannya.
---

# Komponen Arsitektur Android

## Menambahkan Komponen ke Proyek

 Sebelum memulai, kami menyarankan agar Anda membaca [Panduan Arsitektur Aplikasi](https://developer.android.com/topic/libraries/architecture/guide?hl=id) Komponen Arsitektur. Panduan ini berisikan beberapa prinsip yang berguna yang berlaku untuk semua aplikasi Android, dan cara penggunaan Komponen Arsitektur secara bersamaan.

Komponen Arsitektur tersedia di repositori Maven Google. Untuk menggunakannya, Anda harus menambahkan repositori ke project Anda.

 Buka file `build.gradle` untuk **project Anda** \(bukan file untuk aplikasi atau modul Anda\), lalu tambahkan repositori `google()` seperti yang ditunjukkan di bawah ini:

```text
allprojects {
    repositories {
        google()
        jcenter()
    }
}
```

### Mendeklarasikan dependensi <a id="declaring_dependencies"></a>

Buka file `build.gradle` untuk **aplikasi atau modul Anda** dan tambahkan artefak yang Anda perlukan sebagai dependensi. Anda dapat menambahkan dependensi untuk semua Komponen Arsitektur, atau memilih subset.

Lihat petunjuk untuk mendeklarasikan dependensi untuk tiap Komponen Arsitektur pada catatan rilis:

* [Futures \(terdapat di androidx.concurrent\)](https://developer.android.com/jetpack/androidx/releases/concurrent?hl=id)
* [Komponen Lifecycle \(termasuk ViewModel\)](https://developer.android.com/jetpack/androidx/releases/lifecycle?hl=id)
* [Navigation \(termasuk SafeArgs\)](https://developer.android.com/jetpack/androidx/releases/navigation?hl=id)
* [Paging](https://developer.android.com/jetpack/androidx/releases/paging?hl=id)
* [Room](https://developer.android.com/jetpack/androidx/releases/room?hl=id)
* [WorkManager](https://developer.android.com/jetpack/androidx/releases/work?hl=id)

Lihat [rilis AndroidX](https://developer.android.com/jetpack/androidx/versions?hl=id) untuk mengetahui nomor versi terbaru untuk setiap komponen.

Untuk informasi selengkapnya tentang pemfaktoran ulang AndroidX, dan pengaruhnya terhadap paket class dan ID modul ini, lihat [dokumentasi pemfaktoran ulang](https://developer.android.com/topic/libraries/support-library/refactor?hl=id) AndroidX.

### Kotlin <a id="kotlin"></a>

Modul ekstensi Kotlin didukung untuk beberapa dependensi AndroidX. Modul ini memiliki akhiran "-ktx" yang ditambahkan ke namanya. Contoh:

```text
implementation "androidx.lifecycle:lifecycle-viewmodel:$lifecycle_version"
```

menjadi

```text
implementation "androidx.lifecycle:lifecycle-viewmodel-ktx:$lifecycle_version"
```

Informasi selengkapnya, termasuk dokumen untuk ekstensi Kotlin, dapat ditemukan di [dokumentasi ktx](https://developer.android.com/kotlin/ktx?hl=id).

### Data Store

Jetpack DataStore adalah solusi penyimpanan data yang memungkinkan Anda menyimpan key-value pair atau objek yang diketik dengan [buffering protokol](https://developers.google.com/protocol-buffers?authuser=1). DataStore menggunakan coroutine Kotlin dan Flow untuk menyimpan data secara asinkron, konsisten, dan transaksional.

Jika saat ini Anda menggunakan [`SharedPreferences`](https://developer.android.com/reference/kotlin/android/content/SharedPreferences?authuser=1) untuk menyimpan data, sebaiknya bermigrasilah ke DataStore.

### Preferences DataStore dan Proto DataStore <a id="prefs-vs-proto"></a>

DataStore menyediakan dua implementasi yang berbeda: Preferences DataStore dan Proto DataStore.

* **Preferences DataStore** menyimpan dan mengakses data menggunakan kunci. Implementasi ini tidak memerlukan skema yang telah ditetapkan sebelumnya, dan tidak memberikan keamanan jenis.
* **Proto DataStore** menyimpan data sebagai instance jenis data kustom. Implementasi ini mengharuskan Anda untuk menentukan skema menggunakan [buffering protokol](https://developers.google.com/protocol-buffers?authuser=1), tetapi memberikan keamanan jenis.

#### Penyiapan

Untuk menggunakan Jetpack DataStore di aplikasi, tambahkan berikut ini ke file Gradle Anda bergantung pada implementasi yang ingin digunakan:[BERJENIS](https://developer.android.com/topic/libraries/architecture/datastore?authuser=1#berjenis-datastore)[PREFERENSI](https://developer.android.com/topic/libraries/architecture/datastore?authuser=1#preferensi-datastore)

```text
// Typed DataStore (Typed API surface, such as Proto)
dependencies {
  implementation "androidx.datastore:datastore:1.0.0-alpha06"

  // optional - RxJava2 support
  implementation "androidx.datastore:datastore-rxjava2:1.0.0-alpha06"

  // optional - RxJava3 support
  implementation "androidx.datastore:datastore-rxjava3:1.0.0-alpha06"
}
// Alternatively - use the following artifact without an Android dependency.
dependencies {
  implementation "androidx.datastore:datastore-core:1.0.0-alpha06"
}
```

**Catatan:** Jika menggunakan artefak `datastore-preferences-core` dengan Proguard, Anda harus menambahkan aturan Proguard ke file `proguard-rules.pro` secara manual agar kolom Anda tidak dihapus. Anda dapat menemukan aturan yang diperlukan [di sini](https://cs.android.com/androidx/platform/frameworks/support/+/androidx-main:datastore/datastore-preferences/proguard-rules.pro?authuser=1).

#### Menyimpan key-value pair dengan Preferences DataStore

Implementasi Preferences DataStore menggunakan class [`DataStore`](https://developer.android.com/reference/kotlin/androidx/datastore/core/DataStore?authuser=1) dan [`Preferences`](https://developer.android.com/reference/kotlin/androidx/datastore/preferences/core/Preferences?authuser=1) untuk mempertahankan key-value pair sederhana ke disk.

#### Membuat Preferences DataStore <a id="preferences-create"></a>

Gunakan delegasi properti yang dibuat oleh [`preferencesDataStore`](https://developer.android.com/reference/kotlin/androidx/datastore/preferences/package-summary?authuser=1#dataStore) untuk membuat instance `Datastore<Preferences>`. Panggil sekali di tingkat teratas file kotlin, dan akses melalui properti ini di seluruh aplikasi Anda. Hal ini memudahkan Anda mempertahankan `DataStore` sebagai singleton. Atau, gunakan [`RxPreferenceDataStoreBuilder`](https://developer.android.com/reference/kotlin/androidx/datastore/rxjava2/RxDataStoreBuilder?authuser=1) jika Anda menggunakan RxJava. Parameter `name` wajib adalah nama Preferences DataStore.[KOTLIN](https://developer.android.com/topic/libraries/architecture/datastore?authuser=1#kotlin)[JAVA](https://developer.android.com/topic/libraries/architecture/datastore?authuser=1#java)

```text
// At the top level of your kotlin file:
val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "settings")
```

#### Membaca dari Preferences DataStore <a id="preferences-read"></a>

Karena Preferences DataStore tidak menggunakan skema yang telah ditetapkan sebelumnya, Anda harus menggunakan fungsi jenis kunci yang sesuai untuk menentukan kunci untuk setiap nilai yang perlu disimpan dalam instance `DataStore<Preferences>`. Misalnya, untuk menentukan kunci nilai int, gunakan [`intPreferencesKey()`](https://developer.android.com/reference/kotlin/androidx/datastore/preferences/core/package-summary?authuser=1#intPreferencesKey%28kotlin.String%29). Selanjutnya, gunakan properti [`DataStore.data`](https://developer.android.com/reference/kotlin/androidx/datastore/core/DataStore?authuser=1#data) untuk mengekspos nilai tersimpan yang sesuai menggunakan `Flow`.[KOTLIN](https://developer.android.com/topic/libraries/architecture/datastore?authuser=1#kotlin)[JAVA](https://developer.android.com/topic/libraries/architecture/datastore?authuser=1#java)

```text
val EXAMPLE_COUNTER = intPreferencesKey("example_counter")
val exampleCounterFlow: Flow<Int> = context.dataStore.data
  .map { preferences ->
    // No type safety.
    preferences[EXAMPLE_COUNTER] ?: 0
}
```

#### Menulis ke Preferences DataStore <a id="preferences-write"></a>

Preferences DataStore menyediakan fungsi [`edit()`](https://developer.android.com/reference/kotlin/androidx/datastore/preferences/core/package-summary?authuser=1#edit) yang mengupdate data secara transaksional dalam `DataStore`. Parameter `transform` fungsi menerima blok kode tempat Anda dapat mengupdate nilai yang diperlukan. Semua kode dalam blok transformasi diperlakukan sebagai transaksi tunggal.[KOTLIN](https://developer.android.com/topic/libraries/architecture/datastore?authuser=1#kotlin)[JAVA](https://developer.android.com/topic/libraries/architecture/datastore?authuser=1#java)

```text
suspend fun incrementCounter() {
  context.dataStore.edit { settings ->
    val currentCounterValue = settings[EXAMPLE_COUNTER] ?: 0
    settings[EXAMPLE_COUNTER] = currentCounterValue + 1
  }
}
```

#### Menyimpan objek yang diketik dengan Proto DataStore

Implementasi Proto DataStore menggunakan DataStore dan [buffering protokol](https://developers.google.com/protocol-buffers?authuser=1) untuk mempertahankan objek yang diketik ke disk.

#### Menetapkan skema <a id="proto-schema"></a>

Proto DataStore memerlukan skema yang telah ditetapkan sebelumnya dalam file proto di direktori `app/src/main/proto/`. Skema ini menetapkan jenis objek yang Anda pertahankan di Proto DataStore. Untuk mempelajari lebih lanjut penetapan skema proto, baca [panduan bahasa protobuf](https://developers.google.com/protocol-buffers/docs/proto3?authuser=1).

```text
syntax = "proto3";

option java_package = "com.example.application";
option java_multiple_files = true;

message Settings {
  int32 example_counter = 1;
}
```

**Catatan:** Class untuk objek yang disimpan dihasilkan pada waktu kompilasi dari `message` yang ditetapkan dalam file proto. Pastikan Anda telah mem-build ulang project.

#### Membuat Proto DataStore <a id="proto-create"></a>

Ada dua langkah diperlukan untuk membuat Proto DataStore untuk menyimpan objek yang diketik:

1. Tentukan class yang mengimplementasikan `Serializer<T>`, dengan `T` adalah jenis yang ditetapkan dalam file proto. Class penserialisasi ini memberi tahu DataStore cara membaca dan menulis jenis data Anda. Pastikan Anda menyertakan nilai default untuk serialisasi yang akan digunakan jika belum ada file yang dibuat.
2. Gunakan delegasi properti yang dibuat oleh `dataStore` untuk membuat instance `DataStore<T>`, dengan `T` adalah jenis yang ditetapkan dalam file proto. Panggil ini sekali di tingkat teratas file kotlin dan akses melalui delegasi properti ini di seluruh aplikasi Anda. Parameter `filename` memberi tahu DataStore file mana yang akan digunakan untuk menyimpan data, dan parameter `serializer` memberi tahu DataStore nama class serialisasi yang ditentukan di langkah 1.

[KOTLIN](https://developer.android.com/topic/libraries/architecture/datastore?authuser=1#kotlin)[JAVA](https://developer.android.com/topic/libraries/architecture/datastore?authuser=1#java)

```text
object SettingsSerializer : Serializer<Settings> {
  override val defaultValue: Settings = Settings.getDefaultInstance()

  override suspend fun readFrom(input: InputStream): Settings {
    try {
      return Settings.parseFrom(input)
    } catch (exception: InvalidProtocolBufferException) {
      throw CorruptionException("Cannot read proto.", exception)
    }
  }

  override suspend fun writeTo(
    t: Settings,
    output: OutputStream) = t.writeTo(output)
}

val Context.settingsDataStore: DataStore<Settings> by dataStore(
  fileName = "settings.pb",
  serializer = SettingsSerializer
)
```

#### Membaca dari Proto DataStore <a id="proto-read"></a>

Gunakan `DataStore.data` untuk mengekspos `Flow` properti yang sesuai dari objek yang disimpan.[KOTLIN](https://developer.android.com/topic/libraries/architecture/datastore?authuser=1#kotlin)[JAVA](https://developer.android.com/topic/libraries/architecture/datastore?authuser=1#java)

```text
val exampleCounterFlow: Flow<Int> = context.settingsDataStore.data
  .map { settings ->
    // The exampleCounter property is generated from the proto schema.
    settings.exampleCounter
  }
```

#### Menulis ke Proto DataStore <a id="proto-write"></a>

Proto DataStore menyediakan fungsi [`updateData()`](https://developer.android.com/reference/kotlin/androidx/datastore/DataStore?authuser=1#updatedata) yang mengupdate objek yang disimpan secara transaksional. `updateData()` memberi Anda status data saat ini sebagai instance jenis data dan mengupdate data secara transaksional dalam operasi baca-tulis-modifikasi atomik.[KOTLIN](https://developer.android.com/topic/libraries/architecture/datastore?authuser=1#kotlin)[JAVA](https://developer.android.com/topic/libraries/architecture/datastore?authuser=1#java)

```text
suspend fun incrementCounter() {
  context.settingsDataStore.updateData { currentSettings ->
    currentSettings.toBuilder()
      .setExampleCounter(currentSettings.exampleCounter + 1)
      .build()
    }
}
```

#### Menggunakan DataStore dalam kode sinkron

**Perhatian:** Hindari pemblokiran thread pada pembacaan data DataStore jika memungkinkan. Pemblokiran thread UI dapat menyebabkan [ANR](https://developer.android.com/topic/performance/vitals/anr?authuser=1) atau jank UI, dan memblokir thread lainnya yang dapat menyebabkan [deadlock](https://en.wikipedia.org/wiki/Deadlock).

Salah satu manfaat utama DataStore adalah API asinkron, tetapi tidak selalu memungkinkan untuk mengubah kode di sekitarnya menjadi asinkron. Hal ini mungkin terjadi jika Anda menangani codebase yang ada yang menggunakan I/O disk sinkron atau jika Anda memiliki dependensi yang tidak menyediakan API asinkron.

Coroutine Kotlin menyediakan builder coroutine [`runBlocking()`](https://kotlin.github.io/kotlinx.coroutines/kotlinx-coroutines-core/kotlinx.coroutines/run-blocking.html) untuk membantu menjembatani jarak antara kode sinkron dan asinkron. Anda dapat menggunakan `runBlocking()` untuk membaca data dari DataStore secara sinkron. RxJava menawarkan metode pemblokiran di `Flowable`. Kode berikut memblokir thread panggilan hingga DataStore menampilkan data:[KOTLIN](https://developer.android.com/topic/libraries/architecture/datastore?authuser=1#kotlin)[JAVA](https://developer.android.com/topic/libraries/architecture/datastore?authuser=1#java)

```text
val exampleData = runBlocking { context.dataStore.data.first() }
```

Menjalankan operasi I/O sinkron pada UI thread dapat menyebabkan ANR atau jank UI. Anda dapat mencegah masalah ini dengan melakukan pramuat data secara asinkron dari DataStore:[KOTLIN](https://developer.android.com/topic/libraries/architecture/datastore?authuser=1#kotlin)[JAVA](https://developer.android.com/topic/libraries/architecture/datastore?authuser=1#java)

```text
override fun onCreate(savedInstanceState: Bundle?) {
    lifecycleScope.launch {
        context.dataStore.data.first()
        // You should also handle IOExceptions here.
    }
}
```

Dengan demikian, DataStore secara asinkron membaca data dan meng-cache-nya dalam memori. Selanjutnya, pembacaan sinkron menggunakan `runBlocking()` dapat lebih cepat atau menghindari operasi I/O disk sepenuhnya jika pembacaan awal telah selesai.

#### Memberikan masukan

Sampaikan masukan dan ide Anda kepada kami melalui resource berikut:[Issue tracker](https://issuetracker.google.com/issues/new?component=907884&%3Btemplate=1466542&authuser=1) ![](https://developer.android.com/topic/libraries/architecture/images/bug.png?authuser=1)Laporkan masalah agar kami dapat memperbaiki bug.

